# Effekte [Plugin]
* * *
Plugin by: *Official World of War*
v1.0.0
* * *
Effekte ändern Werte einer gesamten Nation.

Effekte können durch Events, bei Gründung der Nation, zufällig von einem Verwalter, etc. hinzugefügt werden.

Beispiele:
- Nation X's Kavallerie erhält +1 Angriff
- Straßen benötigen nun eine Runde mehr um gebaut zu werden!