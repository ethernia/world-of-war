# Searching for the Enlgish Translation?
## There is the official [english-translation](https://gitlab.com/ethernia/world-of-war/-/tree/english-translation) branch!

# About
* * *
Webseite: `ethernia-root.github.io/worldofwar`
Git Repository: `gitlab.com/ethernia/world-of-war`

Official World of War Discord: `dsc.gg/world-war`

SEE DISCORD FOR MORE LINKS WITH MORE INFORMATION!

# Version
v.3.0.0
Dieses Regelwerk wird eventuell nach Spielen verändert.
Beispielsweise wollen wir Winter hinzufügen, aber wir wissen nicht, wann und wie lange ein Winter fair wäre,
Wir werden also das offizielle Spiel (siehe `# Wie kann ich es spielen?`)  analysieren und verändern.

# Lizenz
Das Spiel ist unter der GNU General Public License v3 veröffentlicht!
Eine Kopie der GPL v3 sollte mit dem Regelwerk kommen.

Dieses Spiel wurde erstellt von:
```yaml
Name:        Geholfen mit:            Social:

<< Spieleleiter / Game Creator >>
Ethernia        Idee, Umsetzung, Server     ethernia-root.github.io

<< Inspiriert durch / Inspired by >>
LAUDIX          Inspiration                 dsc.gg/laudix

<< Mitgearbeitet / Helped >>
Lord Flo        Hilfe mit Regeln            Discord: Lord Flo#9204
RussianHusky    Hilfe mit Regeln            Discord: RussianHusky#3822
Michel08        Hilfe mit Plugins           Discord: Michel08#8684
```
Credits:
```yaml
Name:            Name bei Beginn:        Link:

<< Erste Spieler (offizielles Spiel) / First Players (official Game) >>
Ethernia             1. Nikosches Reich              ethernia-root.github.io
LAUDIX               Demokratische Republik Leander  dsc.gg/laudix
Slake                Eine Nation                     Discord: Slake#6573
EulenMensch243       Republik der Eulen              Discord: EulenMensch243#4422
Krafter4             Krafter Imperium                Discord: Krafter4#9846
SpanishInquisition   Republik Spanien                Discord: SpanishInquisition#6211
-Kyle                United States of Kyle           Discord: -Kyle#7175
ViveLaDeala          Asiatische Union                Discord: ViveLaDeala#8558
DaBabey              Japan                           Discord: DaBabey#5932
Kazu                 Italienisches Reich             Discord: Kazu#0110
relol004             Vereinigte Staaten des Balkan   Discord: relol004#7922
LinMax               Max-ismus                       Discord: LinMax#5122
MJF                  Chile                           Discord: MJF#0516
Dave Davidson        Polen-Litauen                   Discord: Dave_Davidson#3124
Lord Flo             Flotopia                        Discord: Lord Flo#9204
GalaxyFan            Imperium                        Discord: GalaxyFan#5177
einfachZaubri        Zaubris Militärische Junta      Discord: einfachZaubri#5605
Michel08             Imperium Insulae                Discord: Michel08#8684
I like roblox guests Elysan Simulami                 Discord: I like roblox guests#9550
jamifre              Freie Amerikanische Republik    Discord: jamifre#8421
Lanina               Russische Reupblik              Discord: 𝐿𝒶𝓃𝒾𝓃𝒶#7328
newmilitarily        Freie Ukraine                   Discord: newmilitarily#6786
nasperokna           Imperium Insulae                Discord: nasperokna#0724
Gavin                Förderation Ostafrika           Discord: Gavin06#9858
```

# [? - About.md](https://gitlab.com/ethernia/world-of-war/-/blob/main/%3F%20-%20About.md) ist, wonach ihr sucht!
