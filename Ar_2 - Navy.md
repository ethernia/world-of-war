# Navy
* * *

# Invasionen
`[< = >]{ [=]  [/] }`  - Landungsschiff mit Infanterie- und Kavalleriedivision geladen!

## Ladung
Landungsschiff in Dock, Truppen auf gleichem Feld --> Truppen können geladen werden!
Max. - 5 Divisionen (keine Extras!)

## Ankunft:
an:
```yaml
Port:			keinen Effekt
Strand:			geschwindigkeit - 1, +1R Wartezeit
Voller Port:		Navy-Schlacht
Port + Inf:		Inf-Schlacht
Voller Port + Inf:	Navy-Schlacht --> Inf-Schlacht
Strand + Inf:		geschwindigkeit - 1, Inf-Schlacht, +1R Wartezeit
```
Invasion --> Angreifer

# Untertauchen
1R: Aufbau
{1R: Ineffektiv}
max. 5 Runden unsichtbar -> 5 Runden Cooldown

Das U-Boot kann nur noch durch Bomben Schaden nehmen
geschwindigkeit +1
Mit Fog of War ist das U-Boot ebenfalls nicht sichtbar.

# Flugzeugträger
`[< - >]{ <X>  </>] }`  - Flugzeugträger mit Jäger und Schlachtflugzeug geladen!

Alle Flugzeuge in einem Flugzäugträger gehen mit dem Schiff unter, wenn es zerstört ist.

Flugzeugträger agieren als mobile Flughafen.
In ihnen können Flugzeuge repariert werden, jedoch nur bis maximal 80% Leben!

## Ladung
Flugzeug auf gleichem Feld wie Fluhzeugträger, Flugzeugträger seit einer Runde nicht bewegt --> Truppen können geladen werden!
Max. - 5 Divisionen (keine Extras!)

## Start
Das starten einer oder mehrerer Flugzeuge dauert eine Runde, in der der Flugzeugträger inaktiv wird.
