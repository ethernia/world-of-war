# Feldlazarett [Plugin]
* * *
Plugin by: *Official World of War*
v2.2.0
* * *
Das Feldlazarett ist eine Division, die hinter Frontlinien Soldaten heilen können!

# Divisionen
Neue Divisionen:
```
* [  +  ] - Feldlazarett
* [ /+\ ] - Feldlazarett (Aufgebaut)

* [  +  ]		0	  0		 0	     1		       25          80	Kann sich Aufbauen
* [ /+\ ]		0	  0		 0	     0		       0          125	Kann sich Abbauen
```

## Feldlazarett-Effekte
Ein Feldlazarett ist eine spezielle Division, die sich zu einem Zelt aufbauen kann, in Zeltform, kann jede Division, die auf dem Feld steht, sich heilen. (Maximale Divisionen: Siehe Bewegung Max.!) 
Das Zelt auf- und abzubauen dauert eine Runde, die Heilung verläuft folgendermaßen:
```diff
Nach 1 Runde:	leben + 5%
Nach 2 Runden:	leben + 10%
Nach 3+ Runden:	leben + 15%
Maximale Heilung: 75%
```
Ein aufgebautes Feldlazarett kann nicht durch Bomben zerstört werden und kann nicht an Frontlinien Platziert werden.
Das Heilen mit einem Feldlazarett benötigt 1 Medizin und jedes Feldlazarett kann bis zu 3 Medizin lagern.

## Infrastruktur
Ein Lazarett kann ein Feld-Lazarett ausbilden.
Die Ausbildung dauert 5 Runden und nutzt 10 Mannstärke!
**Für das Feld-Lazarett ist eine Kette mit Zivil- und Kriegsfabrik benötigt.**
*Ein Feldlazarett kann nur 1 Mal pro 2 Lazaretts in der gesamten Nation ausgebildet werden! (bei nur einem Lazarett, kann eine Division ausgebildet werden, bei dem zweiten nicht, bei dem dritten eine zweite Division, usw.!)*
