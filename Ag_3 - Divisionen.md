# Divisionen
* * *

** = Infanterie-Basierend

# Arten
```diff
: Infanterie:
: [  X  ] - Infanterie**
: [  /  ] - Kavallerie
: [  O  ] - Motor. Infanterie
: [  U  ] - Leichter Panzer
: [  V  ] - Schwerer Panzer
: [  =  ] - Paratrooper**
: [  H  ] - Miliz**
: [  #  ] - Transporter
: [  '  ] - Öl-Transporter

- Navy:
- [< X >] - Zerstörer
- [< / >] - Korvette
- [< O >] - Fregatte
- [< V >] - U-Boot
- [< = >] - Landung
- [< - >] - Flugzeugträger
- [< # >] - Konvoi
- [< ' >] - Öl-Konvoi
- [< ~ >] - Kanalbauschiff

+ Air Force:
+ <  X  > - Jäger
+ <  O  > - Bomber
+ <  /  > - Schlachtflugzeug
+ <  =  > - Transportflugzeug
```

# Werte
```diff
- Art:		Angriff | Verteidigung | Offensive | Geschwindigkeit | Gewicht | Leben | Öl  | Ölverbrauch | Extra
[  X  ]		5	  5		 2	     3		       20         100	 000   0             X
[  /  ]		6	  3		 4	     5		       20         80	 000   0             X
[  O  ]		5	  5		 3	     4		       20         100	 010   0,5           X
[  U  ]		7	  6		 5	     5		       25         125	 010   0,5           X
[  V  ]		8	  8		 5	     3		       30         150	 010   0,5           X
[  =  ]		5	  5		 2	     3		       20         100	 000   0             Kann Para-Ops austragen
[  H  ]		4	  3		 2	     3		       20         80	 000   0             X
[  #  ]		0	  0		 0	     3		       30         0      010   0,5           5 Speicherslots
[  '  ]		0	  0		 0	     3		       30         0      010   0,5           50 Ölslots

[< X >]		7	  6		 3	     3		       0          100	 110   1             X
[< / >]		6	  5		 3	     4		       0          80	 100   1             X
[< O >]		8	  7		 1	     2		       0          150	 130   1             X
[< V >]		5	  4		 3	     4		       0          100	 080   1             Kann Untertauchen und unsichtbar werden
[< = >]		4	  5		 2	     4		       0          100	 120   1             Kann Truppen Laden
[< - >]		3	  5		 0	     4		       0          100	 120   1             Kann Flugzeuge Laden
[< # >]		0	  0		 0	     4		       0          0      100   1             15 Speicherslots
[< ' >]		0	  0		 0	     4		       0          0      100   1             200 Ölslots
[< ~ >]		0	  0		 0	     4		       0          0      100   2             Kann Kanalsegmente konstruieren (verbraucht 5 Kohle und 1 Stahl)

<  X  >		5	  5		 3	     3		       0          100	 050   1             X
x  O  >		4	  5		 2	     3		       0          70	 050   1,5           Kann Bomben
<  /  >		6	  5		 3	     4		       0          100	 050   1,5           X
<  H  >		4	  6		 0	     3		       0          100	 070   1             Kann Para-Ops austragen
```
