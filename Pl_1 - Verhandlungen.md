# Verhandlungen
* * *
Es kann jederzeit verhandelt werden.
*Im offiziellen Spiel wird dies über den Handlungsraum oder die Vereinten Nationen getan.*

In Verhandlungen kann eine Nation einer anderen beispielsweise Land, Mannstärke, Bauarbeiter, Divisione, usw. anbieten.
In Verhandlungen kann natürlich auch Frieden, Krieg, usw. unter Bedingungen angeboten werden.

## Diplomaten:
Im offiziellen Spiel sind:

- Jeder Fraktionsführer
- ein weiterer Spieler aus jeder Fraktion
- jede neutrale Nation

Diplomaten.

Diplomaten haben Zugriff auf die Vereinten Nationen, die für Koordination gemeinsamer Ziele nutzt wird.

# Friedensverhandlungen
Friedensverhandlungen beenden einen Krieg sofort, mit einer Verhandlung.

Eine Frieednsverhandlung passiert im offiziellen Spiel dann, wenn:

- eine Nation kapituliert
- das Staatsoberhaupt gefangen wurde
- 100% des Landes einer Nation besetzt sind
