# Bewegung
* * *

# eine Division
```
Geschwindigkeit:

1 - 1 Feld   / 3 Runden	(0.33 Felder/R)
2 - 1 Feld   / 2 Runden	(0.5 Felder/R)
3 - 1 Feld   / 1 Runde	(1 Feld/R)
4 - 3 Felder / 2 Runden	(1.5 Felder/R)
5 - 2 Felder / 1 Runde	(2 Felder/R)
```

SP = Geschwindigkeits-Wert
X = Division nicht bewegt
1 = um ein Feld bewegt
2 = um zwei Felder bewegt
```
		Runde:	0	1	2	3	4	5	6	7	8	9
SP:

 1:		X	X	X	1	X	X	1	X	X	1
 
 2:		X	X	1	X	1	X	1	X	1	X
 
 3:		X	1	1	1	1	1	1	1	1	1
 
 4:		X	1	2	1	2	1	2	1	2	1
 
 5:		X	2	2	2	2	2	2	2	2	2
```

(Geschwindigkeit 0 = n. bewegbar!)

# mehrere Divisionen

Jedes Feld hat eine Größe von 100.

Das Gewicht einer Division wird subtrahiert, es kann nicht mehr als 100 Gewicht auf einem Feld stehen!

Bewegt man mehrere Divisionen mit unterschiedlichen Geschwindigkeitswerten gleichzeitig, kann man:
- allen Divisionen die Geschwindigkeit der langsamsten zu geben, um sie zusammen zu bewegen
- jede Division mit ihrer eigenen Geschwindigkeit bewegen

# Öl
Hat eine Division mit >0 Ölverbrauch 0 Öl, ist ihre Geschwindigkeit 0.
Ein Flugzeug mit 0 Öl stürzt auf das darunterliegende Feld ab. Ist dies Wasser, ist es zerstört.
Man kann abgestürzte Flugzeuge genau so, wie Schiffe und Infanteriedivisionen, wieder mit Öltankern befüllen.

# Einnehmen
Bewegt sich eine feindliche Division auf einen Transporter, übernimmt sie diese.
Ein Feind kann auch eine Navy- oder Air-Force Division, sowie eine Infanteriedivision, die ohne Öl frei steht, befüllen und damit Übernehmen. Dies kostet Leben% an Mannstärke%.

# Defensive Linien
Der Auf- und Abbau einer Defensiven Linie dauert 1R, sie zu Verlassen ebenfalls.
Das Verlassen bringt ebenfalls:
```diff
geschwindigkeit -1
offensive - 15%
```
für 3 Runden.
Folgende Divisionen können eine Defensive Linie betreten:
: [  X  ] - Infanterie
: [  =  ] - Paratrooper
: [  H  ] - Miliz
und sie erhalten folgende Werte:
```
geschwindigkeit => 0
verteidigung + 1
offensive - 20
belieferung +1 Feld von Straße entfernt
```
