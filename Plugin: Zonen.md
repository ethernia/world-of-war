# Zonen [Plugin]
* * *
Plugin by: *Official World of War*
v2.1.0
* * *
Zonen sind Regionen auf Karten, die dem Spieler Effekte verleihen können

# Karte
Zonen werden auf der Karte verteilt.

## Kälte
```diff
Größe: 100

+ Verteidiger:
+ verteidigung		-15%

- Angreifer:
- offensive		-15%

Extra:
*Erfrieren:* Nach 3 Runden in Kältezone,
Würfel 1d6:
1 - 2:	Nichts Passiert
3 - 4:	leben - 20%
5:	leben - 60%
6:	leben - 100%

Farm: -0,5 Output
```

(i:) *Winter kommt in einer späteren Version des Regelwerks. Wir müssen erst Spiele analysieren um herauszufinden, wann und wie lange ein Winter fair ist!*

## Berg
```diff
Größe: 70

+ Verteidiger:
+ verteidigung		+10%

Panzer:
geschwindigkeit - 1

- Angriff von max. 3 Seiten!

Kohlemine: +0,5 Output
```

## Jungel
```diff
Größe: 90

: Neutral:
: geschwindigkeit	-1

- Angreifer:
- angriff 		-20%

- Angriff von max. 3 Seiten!
```

# Fluss
Ein Fluss liegt *zwischen* Feldern!
Ein Fluss kann nur mit Brücke überquert werden!

```
: Anliegende Stadt: + 2/R Mannstärke

- Angreifer:
- angriff       -10%

Panzer:
geschwindigkeit - 1
```

## Unbewegbar
Größe: 0

(i:) *Wie kann diese Zone eingenommen werden? Normalerweise, gar nicht. Sie kann in Verhandlungen aber zugewiesen werden!*

(Plugin-Kompatibilität: Imperium Insulae:) *Das Imperium Insulae besitzt eine unbewegbare Zone, wenn es alle angrenzenden Zonen besetzt!*

Das Feldlazarett ist eine Division, die hinter Frontlinien Soldaten heilen können!

# Divisionen

** = Infanterie-Basierend

Neue Divisionen:
```
: [  !  ] - Spezialeinheit**
::[ *!* ] - Spezialeinheit** (Kältezone)
::[ ^!^ ] - Spezialeinheit** (Bergzone)
::[ -!- ] - Spezialeinheit** (Jungelzone)

: [  !  ]		5	  5		 2	     3		       100	X
::[ *!* ]		5	  5		 2	     3		       100	Immun gegen Effekte von Kältezonen
::[ ^!^ ]		5	  5		 2	     3		       100	Immun gegen Effekte von Bergzonen als Angreifer (-10% verteidigung gegner)
::[ -!- ]		5	  5		 2	     3		       100	Immun gegen Effekte von Jungelzonen
```

(Spezialeinheiten können Defensive Linien betreten!)
