# Innenpolitik [Plugin]
* * *
Plugin by: *Official World of War*
v1.0.0
* * *
Jede Nation hat ein Volksmeinungslevel..
Alle 5 Runden fordert das Volk Änderungen. Diese Forderungen können Nachteile, sowie Vor- und Nachteile haben. Akzeptiert man die Forderungen, gewinnt man an Volksmeinung, lehnt man sie ab, verliert man Volksmeinung. Politische Events und Entscheidungen können ebenfalls die Volksmeinung beeinflussen.
Großevents sind mehrere aneinanderfolgende Events, die sich gegenseitig beeinflussen. Sollte der Spielverwalter selbst ein Mitspieler sein, werden seine Entscheidungen bei Großevents ausgewürfelt, sodaß er nicht die jeweils beste Option wählen kann.

Volksmeinungslevel:
```diff
- [+4] PERFEKT: -1 Forderung / Runde
- [+3] GROßARTIG: +2 Mannstärke / Runde
- [+2] GUT
- [+1] POSITIV
- [+0] NEUTRAL << Standard! >>
- [-1] NEGATIV
- [-2] SCHLECHT
- [-3] GRAUSAM: -5 Mannstärke / Runde
- [-4] SCHRECKLICH: +1 Forderung / Runde
- [-5] TYRRANEI: <Führer wird gestürzt / abgewählt>
```
Stürzen / Abwählen:
Das Stürzen / Abwählen kann je nach Spielweise einen anderen Effekt haben.
Beispielsweise könnte ein anderer Mitspieler die Nation übernehmen, oder der aktuelle Spieler, wie im offiziellen Spiel, muss für 25 Runden allen Forderungen des Volkes nachgehen.

# Beispiel-Events
### STREIK
+2 Runden Bauzeit für 1-6 zufällige Fabriken/Werften/Flughäfen/Kasernen/Ports/Lazaretts
### HUNGER
+1 Nahrungsverbrauch über 3 Runden für 1-6 zufällige Städte
### KRANKHEITSAUSBRUCH
x2 Medizinverbrauch über 5 Runden
### INDUSTRIALISIERUNG*
3 Bauarbeiter pausieren ihre Arbeit und konstuieren (GEBÄUDE) neben einer zufälligen Stadt
### INFRASTRUKTUR-AUFWERTUNG*
1-6 Bauarbeiter pausieren ihre Arbeit und starten 1-2R Reperaturen an Straßen / Städten
### ÖLPREISSENUNG
Öl-Raffinerien stellen nur noch 5 Öl / 2 Runden her
*(Optional: 1-6 zufällige Ölsilos verlieren 10-60 Öl)*
### DEMILITARISIERUNG
6 zufällige Divisionen werden aufgelöst, Fahrzeuge -> Nationales Lager, Waffen & Öl -> entfernt
