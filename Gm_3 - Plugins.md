# Plugins
* * *
Wie erwähnt, kann jeder in seinem Spiel eigene Regeln festlegen.
Plugins sind genau das, nur eine offizielle Version!

Wenn jemand ein gutes Set an Extra-Regeln erstellt hat, kann es eingereicht werden um als Plugin im Repo gelistet werden, sodass jeder diese optionalen Regeln sieht, und anwenden kann!

Plugins sind hier mit dem Prefix "Plugin:" unter den Regeln gelistet, der Name der Plugins endet mit dem Suffix "[Plugin]".

Plugins können auch vom offiziellen Team erstelt werden, sodass jeder weiß, dass es optional ist.

* * *
Das offizielle Spiel nutzt die folgenden Plugins:
- Innenpolitk
- Bürgerkriege
- Doktrien
- Effekte
- Feldlazarett
- Imperium Insulae
- Kalender
- Terrorismus (wird erst mit den "Moderne Kriegsführung"-Zeitaltern aus dem Kalender Plugin genutzt!)
- Zonen
