# Fraktionen
* * *
Im offiziellen Spiel tritt man einer Fraktion bei, sowie erstellt man eine Fraktion in den Vereinten Nationen.
Der Beitritt und das Kicken einer Fraktion kann von einer Mehrheit der Mitglieder abgelehnt werden.

Der Austritt, eine Nation herauswerfen, den Fraktionsführer zu ändern, den Namen zu ändern, usw. wird auch über die Vereinten Nationen angekündigt!

# Kriege
Divisionen können durch alliiertes Land bewegt werden. Greift man vom Land eines Verbündeten aus an, ist der Verbündete ebenfalls in den Krieg verwickelt!
Nationen in der gleichen Fraktion können sich gegenseitig Divisionen austauschen, steht eine Division an einer Grenze, kann sie die Zugehörigkeit ändern und dem Verbündeten beitreten.
Dies kann auch über Verhandlungen zeitbegrenzt durchgeführt werden!
Nationen in der gleichen Fraktion können sich auch nicht gegenseitig angreifen.
Ein Fraktionsmitglied tritt nicht automatisch dem Krieg bei.

# Infrastruktur & Armeen
Eine Nation kann, wenn die andere Partei zustimmt, Infrastruktur und Divisionen im Land eines Verbündeten zu erstellen, diese stehen dann unter Kontrolle der anderen Partei.

# Staatenbunde
Zwei oder Mehrere Nationen können einen Staatenbund gründen,
Dabei behält jedes Mitglied Kontrolle über die eigene Armee, Infrastruktur, etc., doch Außenpolitik wird von einer Partei vertreten.
Staaten können anderen Staaten im Staatenbund den Krieg erklären. Nach jeder Friedensverhandlung, die die Vertreter-Partei beinhaltet, kann die Vertreter-Partei auf einen anderen Staat übergehen. Sollte sich nicht geeinigt werden können, zerfällt der Staatenbund. Natürlich kann der Vertreter eine Gemeinschaftliche Außenpolitik ausrufen und bestimmte oder alle Staatenführer nach dessen Meinung fragen, de facto kontrolliert der Hauptstaat die Außenpolitik.
