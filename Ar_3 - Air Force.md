# Air Force
* * *

# Effektivitäts-Bereiche
H: Horizontal + Vertikal
D: Diagonal
```diff
- 6H + 5D: Luftangriffe
+ 4H + 3D: Unterstützung, Bomben
: 2H + 1D: Para-Ops
```

# Angriffe
2 Zufällige Flieger / Bereichsgruppe,
Paratrooper:
```diff
Würfel 1d6:

1 - 3:	Division landet in der 3. Zone (Zufällige Pos.) + leben - 10%
4:	Division landet in der 3. Zone (Zufällige Pos.) + leben - 50%
5 - 6:	leben - 100%

(wenn in Wasser: leben - 100%)
```

# Unterstützung
Jede Runde:
```diff
Würfel 1d6:

1 - 2:	Keinen Effekt
3 - 4:	Zufällige eigene Division angriff	+10%(1R)
5:	Zufällige gegnerische Division leben	-15%
6:	Zufällige gegnerische Division leben	-30%
```

# Bomben
Bombenaktionen können Infrastruktur zerstören, sodass sie Repariert werden muss,
sie können aber auch Navy-Divisionen zerstören!
(Wird ein Flugzeugträger bombardiert, starten alle geladenen Divisionen und starten sofort eine defensive Schlacht, nach der bei Überleben des Bombers die Aktion ausgeführt wird!)
Hauptstädte können nicht Bombadiert werden!
Das Bomben hat einen Cooldown von 5 Runden.
Bombenaktion:
```diff
Würfel 1d6

1 - 2:	Angriff findet nicht statt.
3 - 4:	Infrastruktur zerstört, Divisionen auf Feld leben - 60%
5 - 6:	Infrastruktur zerstört, Divisionen auf Feld leben - 100% {Navy-Division zerstört}
```
Eine Bombenaktion kann 2 Mal pro Runde durchgeführt werden, alles darüber senkt Kriegsunterstützung!

# Para-Ops
`< = >{ [=] }`  - Transportflugzeug mit Paratrooper-Division geladen!

## Ladung
Transportflugzeug in Flughafen, Truppen auf gleichem Feld --> Truppen können geladen werden!
Max. - 2 Divisionen (keine Extras!)

## Ankunft:
Invasion --> Angreifer

# Kamikaze
```diff
Würfel 1d6

1: Angriff findet nicht statt.
2 - 3: 60% Schaden
4 - 5: 75% Schaden
6: 100% Schaden
```
Eine Kamikazeaktion kann 1 Mal pro 5 Runden durchgeführt werden, alles darüber senkt Kriegsunterstützung!