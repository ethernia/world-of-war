# Kalender [Plugin]
* * *
Plugin by: *Official World of War*
v1.1.0
* * *
Das Spiel wird in Zeitaltern gespielt, diese werden durch Events verändert.
Jedes Zeitalter hat seine eigenen Technologien, Möglichkeiten und Politik, die zu kompliziert wäre, um sie hier aufzuzählen, aber sie orientiert sich an der Realität!

* * *
* * *
* * *
# Aufbauphase
Nationen werden erstellt, die Karte wird gefüllt, mdst. Fraktionen werden erstellt und Regeln werden festgelegt!
Ist das erledigt, können Nationen Politik aushandeln, Pläne schmieden, usw.
Erklären sich die ersten beiden Fraktionen einen Krieg, beginnt das Spiel!
* * *
### Event: Der erste Kontakt - Krieg zwischen 2 Fraktionen entsteht
2 Fraktionen erklären sich gegenseitig den Krieg. Lasst die Spiele beginnen!
## Zweiter Weltkrieg

### Event: Kriegsende - Weltfrieden!
Die Welt kämpft keine aktiven Konflikte. Neue Technologien und Politik entstehen.
## Moderne Kriegsführung

### Event: EXO-tischer Fortschritt - Exo-Skellete erforscht
Krieg ist nicht mehr Effektiv, es ist nicht möglich einen Soldaten zu töten, Alternative? Technologie!
## Kriegsführung der Zukunft

### Event: Der große Filter - BlakBomb wird getestet
Die erste 'BlakBomb' wird getestet. Leider hatte sie einen Fehler, sie hat alles Leben ausgelöscht... außer...
* * *
# 2. Aufbauphase
erneute Aufbauphase, eine Großmacht und viele kleine Nationen werden erstellt!
* * *
## Antike

### Event: Dunkele Zeiten - Großmacht wird vernichtet
Die originale Großmacht wird überrannt, doch was nun? Ein neues Zeitalter entsteht.
## Mittelalter

### Event: Neue Welt - Amerika entdeckt
Die Neue Welt wird durch Politische Events entdeckt. Neue Technologien werden erforscht und die Karte wird um Amerika und Australien erweitert.
## Kolonialismus

### Event: Kontakt - Krieg zwischen 2 Fraktionen entsteht
2 Fraktionen erklären sich gegenseitig den Krieg.
## Großer Krieg

### Event: Der erste Kontakt - Krieg zwischen 2 Fraktionen entsteht
2 Fraktionen erklären sich gegenseitig den Krieg. Lasst die Kapitel 2 beginnen!
## Zweiter Weltkrieg (Kapitel 2)

### Event: Kriegsende - Auslösender Krieg ist beendet
Die Welt kämpft keine aktiven Konflikte. Neue Technologien und Politik entstehen.
## Moderne Kriegsführung (Kapitel 2)

### Event: EXO-tischer Fortschritt - Exo-Skellete erforscht
Krieg ist nicht mehr Effektiv, es ist nicht möglich einen Soldaten zu töten, Alternative? Technologie!
## Kriegsführung der Zukunft (Kapitel 2)

### Event: Großartige Einheit - Alle Menschen sind unter einem Banner vereint!
Die Menschen erkennen, friedlich oder durch Krieg, dass Krieg nicht effektiv ist um unsere Ziele zu erreichen, dass der Mensch als Spezies noch nicht reig genug für all diese Macht ist und dass Politiker sich nur wie kleine Kinder an ihre egoistischen Ziele klammern. Es wird keine Kriege mehr geben, die Menschheit wird großes erreichen!

# das Ende!
## World of War ist beendet. Danke für's Mitspielen!
