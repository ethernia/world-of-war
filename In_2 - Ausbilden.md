# Ausbilden
* * *

# Infanterie
Eine Infanterie-Division kann in einer Kaserne ausgebildet werden.
Hier ist eine Liste mit allen Runden, Ressourcen und Mannstärke, die benötigt sind um Divisionen auszubilden!
Infanterie-basierte Divisionen starten mit 50 Waffen.
Diese Aktionen kosten `1 Kohle`.
```diff
- Art:		Runden:		Mannstärke:		Ressourcen:
[  X  ]		10		10		        X
[  /  ]		10		10		        X
[  O  ]		10		15		        1 Auto
[  U  ]		20		20		        1 Panzer
[  V  ]		25		30		        1 Panzer
[  =  ]		10		10		        X
[  #  ]		10		10		        1 Auto
[  '  ]		10		10		        1 Auto
[ *!* ]		15		15		        X
[ ^!^ ]		15		15		        X
[ -!- ]		15		15		        X
```
Es werden immer 3 Divisionen gleichzeitig ausgebildet!

# Navy
Eine Navy-Division kann in einem Port ausgebildet werden.
Hier ist eine Liste mit allen Runden, Ressourcen und Mannstärke, die benötigt sind um Divisionen auszubilden!
Diese Aktionen kosten `1 Kohle`.
```diff
- Art:		Runden:		Mannstärke:		Ressourcen:
[< X >]		20		10		        1 Zerstörer
[< / >]		20		10		        1 Korvette
[< O >]		25		15		        1 Fregatte
[< V >]		25		20		        1 U-Boot
[< = >]		20		10		        1 Landungsschiff
[< - >]		20		15		        1 Flugzeugträger
[< # >]		10		10		        1 Konvoi
[< ' >]		10		10		        1 Konvoi
[< ~ >]		20		30		        1 Kanalbauschiff
```
Es werden immer 2 Divisionen gleichzeitig ausgebildet, ausser bei dem Kanalbauschiff!

## Rückbau:
Schiffe in einem Port können umgebaut werden.
Dies dauert 3 Runden und kostet genau so viel wie das eigentliche Schiff. Die ursprünglichen Ressourcen gehen verloren.
Dies kostet ebenfalls `1 Kohle`.

# Air Force
Eine Air Force-Division kann in einem Flughafen ausgebildet werden.
Hier ist eine Liste mit allen Runden, Ressourcen und Mannstärke, die benötigt sind um Divisionen auszubilden!
Diese Aktionen kosten `1 Kohle`.
```diff
- Art:		Runden:		Mannstärke:		Ressourcen:
<  X  >		20		10		        1 Jäger
<  O  >		25		15		        1 Bomber
<  /  >		20		15		        1 Schlachtflugzeug
<  =  >		25		10		        1 Transportflugzeug
```
Es werden immer 2 Divisionen gleichzeitig ausgebildet!
