# Bürgerkriege [Plugin]
* * *
Plugin by: *Official World of War*
v1.2.0
* * *

# Start
Ein Bürgerkrieg kann mit dem betreten einer Nation trotz gefüllter Karte entstehen!
Es gibt verschiedene Arten von Bürgerkriegen:
```yaml
- Revolutionen      -   Bürgerkrieg innerhalb einer Nation, die diese eine Nation Reformieren möchte.
- Landforderungen   -   Bürgerkrieg innerhalb einer Region, die diese Region für sich beansprucht.
- Kriegsforderungen -   Bürgerkrieg innerhalb einer Nation/Fraktion, die Forderungen stellt, wie beispielsweise das Ende eines Krieges.
```
## Revolutionen:
Revolutionen können in Jeder Stadt einer Nation, außer der Hauptstadt, starten.
Sie werden erst beendet, wenn die gesamte Nation gefallen ist (erstellt eine Militärische Junta bis zur Konstitution) oder wenn der Bürgerkrieg fehlschlägt.

## Landforderungen:
Landforderungen starten in einer Stadt (außer Hauptstädte), unabhängig von der Nation,
Sie werden erst beendet, wenn sie das beanspruchte Land mit einer Friedensverhandlung erlangt haben oder wenn der Bürgerkrieg fehlschlägt.

## Kriegsforderungen:
Kriegsforderungen können in mehreren Städten gleichzeitig starten, solang sie in der gleichen Fraktion sind.
Kriegsforderungen werden meist durch Politische Events gestartet, beispielsweise wenig Kriegsunterstützung.
Sie werden erst beendet, wenn die Fraktion fällt, was eine Friedensverhandlung hervorruft, oder wenn der Aufstand unterdrückt wurde.

# Offizielle Richtlinien
Im Offiziellen Spiel funktionieren Bürgerkriege folgendermaßen:
- Es gibt nur einen Bürgerkrieg gleichzeitig
- Der Beginn des Bürgerkrieges dauert 3 Runden
- Der Aufstand erhält:
   - 30% der Armeen der Kriegsparteien
   - +1 Miliz pro:
      - Kaserne
      - Gesamtsiegespunkt
      - Stadt
- Niemand kann der Aufstandspartei Krieg erklären
- Bürgerkriege können Dynamisch angepasst werden
