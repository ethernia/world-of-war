# Runden
* * *
*(Diese Seite zeigt die offiziellen Regeln bezüglich Runden für das offizielle Spiel, wenn das Spiel beispielsweise als Brettspiel gespielt wird, treffen diese Regeln nicht vollständig zu!)*
Die erste Runde beginnt um 17 Uhr, die zweite um 18 Uhr und die letzte um 19 Uhr.
Die Spieler haben eine Stunde Zeit um auf ihre Runden zu Antworten.
Am Anfang jeder Runde erhält jeder Spieler einen Report, was passiert ist!

# Reihenfolge
Die Spieler werden in der Reihenfolge der Liste der Nationen an der Reihe sein!
Die Nation oben beginnt, die unten beendet die Runde, danach startet die nächste Runde!

# Runden
## Kriegsrunde
In der Kriegsrunde können Divisionen bewegt werden und Aktionen ausführen!

## Logistikrunde
In der Logistikrunde können Gebäude gebaut, Divisionen ausgebildet werden, etc.!

## Politikrunde
In der Politikrunde kann Politik verhandelt werden.
In der Politikrunde werden auch Events gegeben,
bsw. einen Aufstand im Volk,
Streik einer Armee, 
etc.

Diese können permanente auswirkungen haben,
bsw. eine Revolution,
mehr Kriegsunterstützung,
etc.

*Polititsche Events können durch andere Ausgelöst werden, ansonsten entscheidet der Spiele-Manager, was für Events stattfinden und was sie auslösen! (Bleibt fair und bleibt realistisch!)*

Im offiziellen Spiel haben wir eine Hintergrundsgeschichte für die ganze Welt hinzugefügt. Diese beeinflusst auch Politische Events, ist aber nicht benötigt um das Spiel zu spielen!
Im offiziellen Spiel gibt es auch "Die Zeit News" um den Rollenspielaspekt hervorzuheben!

# Ende
Sind alle Runden beendet, können Spieler weiter Verhandeln, etc., bis zum Beginn der nächsten Runde
hier 
