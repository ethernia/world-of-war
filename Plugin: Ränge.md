# Ränge [Plugin]
* * *
Plugin by: *Official World of War*
v1.0.0
* * *
Nach einer bestimmten Anzahl an Kills einer Division, steigt sie einen Rang auf!

(i:) *Wenn mehrere Divisionen eine andere besiegen, haben alle Divisionen diese Division besiegt. Wenn eine Division als Kriegsgefangene genommen wird, hat die Division, die zuletzt beteiligt war, diese Division besiegt.*

# Liste aller Ränge
#### `( ^ )` - Corporal
**Benötigte Kills:** 5
```yaml
anrgiff +		0
verteidigung +		0
offensive +		5%
geschwindigkeit +	0
```

#### `( O )` - Sergeant
**Benötigte Kills:** 10
```yaml
angriff +		5%
verteidigung +		5%
offensive +		5%
geschwindigkeit +	0
```

#### `( - )` - Lieutenant
**Benötigte Kills:** 15
```yaml
angriff +		5%
verteidigung +		10%
offensive +		5%
geschwindigkeit +	0
```

#### `( | )` - Colonel
**Benötigte Kills:** 20
```yaml
angriff +		10%
verteidigung +		10%
offensive +		5%
geschwindigkeit +	0
(Plugin-Kompatibilität: Zonen:)
Nicht von Jungel-Zonen beeinträchtigt!
```

#### `( = )` -  Major
**Benötigte Kills:** 25
```yaml
angriff +		10%
verteidigung +		10%
offensive +		5%
geschwindigkeit +	0
(Plugin-Kompatibilität: Zonen:)
Nicht von Jungel-Zonen beeinträchtigt!von Geschwindigkeits-Defiziten beeinträchtigt!
```

#### `( * )` - General
**Benötigte Kills:** 30
```yaml
angriff +		10%
verteidigung +		10%
offensive +		5%
geschwindigkeit +	1 (wenn 5, ignoriert)
(Plugin-Kompatibilität: Zonen:)
Nicht von Jungel-Zonen beeinträchtigt!
Nicht von Geschwindigkeits-Defiziten beeinträchtigt!
```
