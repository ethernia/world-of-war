# Hauptstädte
* * *

# Jede Nation startet mit 100 Manstärke!

# Kapitulation
Ist die Hauptstadt einer Nation gefallen, gibt es 3 Mögliche Enden;
```diff
Würfel 1d6:

1 - 3:	Hauptstadt wird an andere Position verlegt, Politisches Event wird ausgelöst!
4 - 5:	Nation Kapituliert, Armeen kämpfen weiter (Wenn Hauptstadt gerettet, keine Kapitulation)
6:	Nation Kapituliert
```

# Invasion
Eine Hauptstadt ist schwerer zu Fall zu bringen
```diff
- Angreifer:
- offensive		-20%

+ Verteidiger:
+ verteidigung		+15%
```

# Navy-Invasion
```diff
Würfel 1d6:

1 - 5:	Invasion schlägt fehl
6:	Invasion wird durchgeführt

- [<[]>] angriff - 10%
- [ [] ]   angriff - 10%
```

# Para-Ops
Para-Ops können durchgeführt werden, jedoch nur in einem Feld von `1H + 0D`,
außerdem gilt eine Neue Kapitulationsregel:
```diff
Würfel 1d6:

1 - 4:	Hauptstadt wird an andere Position verlegt, Politisches Event wird ausgelöst!
5 - 6:	Nation Kapituliert, Armeen kämpfen weiter (Wenn Hauptstadt gerettet, keine Kapitulation)
```

# Hauptstadt Bewegen
Die Hauptstadt einer Nation kann in einem Krieg nicht bewegt werden.
Die Hauptstadt zu bewegen ist ansonsten kostenfrei und dauert eine Runde.
