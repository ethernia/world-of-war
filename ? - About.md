# About
* * *
Webseite: `ethernia-root.github.io/worldofwar`
Git Repository: `gitlab.com/ethernia/world-of-war`

Official World of War Discord: `dsc.gg/world-war`

SEE DISCORD FOR MORE LINKS WITH MORE INFORMATION!

# Version
v.3.0.0
Dieses Regelwerk wird eventuell nach Spielen verändert.
Beispielsweise wollen wir Winter hinzufügen, aber wir wissen nicht, wann und wie lange ein Winter fair wäre,
Wir werden also das offizielle Spiel (siehe `# Wie kann ich es spielen?`)  analysieren und verändern.

# Lizenz
Das Spiel ist unter der GNU General Public License v3 veröffentlicht!
Eine Kopie der GPL v3 sollte mit dem Regelwerk kommen.

Dieses Spiel wurde erstellt von:
```yaml
Name:        Geholfen mit:            Social:

<< Spieleleiter / Game Creator >>
Ethernia        Idee, Umsetzung, Server     ethernia-root.github.io

<< Inspiriert durch / Inspired by >>
LAUDIX          Inspiration                 dsc.gg/laudix

<< Mitgearbeitet / Helped >>
Lord Flo        Hilfe mit Regeln            Discord: Lord Flo#9204
RussianHusky    Hilfe mit Regeln            Discord: RussianHusky#3822
Michel08        Hilfe mit Plugins           Discord: Michel08#8684
```
Credits:
```yaml
Name:            Name bei Beginn:        Link:

<< Erste Spieler (offizielles Spiel) / First Players (official Game) >>
Ethernia             1. Nikosches Reich              ethernia-root.github.io
LAUDIX               Demokratische Republik Leander  dsc.gg/laudix
Slake                Eine Nation                     Discord: Slake#6573
EulenMensch243       Republik der Eulen              Discord: EulenMensch243#4422
Krafter4             Krafter Imperium                Discord: Krafter4#9846
SpanishInquisition   Republik Spanien                Discord: SpanishInquisition#6211
-Kyle                United States of Kyle           Discord: -Kyle#7175
ViveLaDeala          Asiatische Union                Discord: ViveLaDeala#8558
DaBabey              Japan                           Discord: DaBabey#5932
Kazu                 Italienisches Reich             Discord: Kazu#0110
relol004             Vereinigte Staaten des Balkan   Discord: relol004#7922
LinMax               Max-ismus                       Discord: LinMax#5122
MJF                  Chile                           Discord: MJF#0516
Dave Davidson        Polen-Litauen                   Discord: Dave_Davidson#3124
Lord Flo             Flotopia                        Discord: Lord Flo#9204
GalaxyFan            Imperium                        Discord: GalaxyFan#5177
einfachZaubri        Zaubris Militärische Junta      Discord: einfachZaubri#5605
Michel08             Imperium Insulae                Discord: Michel08#8684
I like roblox guests Elysan Simulami                 Discord: I like roblox guests#9550
jamifre              Freie Amerikanische Republik    Discord: jamifre#8421
Lanina               Russische Reupblik              Discord: 𝐿𝒶𝓃𝒾𝓃𝒶#7328
newmilitarily        Freie Ukraine                   Discord: newmilitarily#6786
nasperokna           Imperium Insulae                Discord: nasperokna#0724
Gavin06              Förderation Ostafrika           Discord: Gavin06#9858
```

# Was ist World of War?
World of War ist ein Strategiespiel.
Es ist quasi eine simpele Version von bsw. Hearts of Iron, nur einige, haben nicht die nötigen Vorraussetzungen um mit uns zu spielen, für diese Leute habe ich dieses Spiel erstellt, sodass wir alle gemeinsam spielen können!

# Wie kann ich es spielen?
Die offizielle Variante kann im World of War Discord gespielt werden, der Server ist aber auch eine Community!

Natürlich könnt ihr aber auch eure eigene Community erstellen, auf egal welcher Plattform, ihr könnt es theoretisch auch bsw. als Brettspiel spielen! Es ist gratis und open-source, sodass jeder es spielen kann, das ist das Ziel von World of War!
In diesen Fällen, könnt ihr natürlich frei die Regeln ändern! Ihr könnt diese Regeln auch wieder veröffentlichen und auch damit Geld verdienen. Siehe `# Lizenz` für mehr Informationen!

# Wie kann ich Plugins oder Änderungen vorschlagen?
Hierfür, schaut im offiziellen Server nach! (Link oben!)
Dort könnt ihr über den Modmail-Bot "Flag & Feedback" etwas vorschlagen!
Alternativ, könnt ihr eine Fork des Gitlab-Repositorys erstellen, Regeln ändern und eventuell werden wir diese Regeln aufnehmen!

# Musik Playlists
**World of War Vol. 1:** https://youtube.com/playlist?list=PLx-MZOWxrfPbEntJItgtGJHV1jfrEy1yv

**World of War Vol. 2:** https://youtube.com/playlist?list=PLx-MZOWxrfPZPUDPxna0RACWwB5AiM3O9

## Was ist der Unterschied?
Volume 2 hat ähnliche Lieder zu Volume 1, nur denke ich sie passen nicht ganz zu den anderen.
Die hinzugefügten Lieder sind ganz oben, alles über Preußens Gloria ist Extra.
Ihr könnt in die hinzugefügten Lieder reinhören und entscheiden ob ihr sie mögt oder nicht.
Im Warteraum auf dem offiziellen Discord wird nur Volume 1 gespielt!
