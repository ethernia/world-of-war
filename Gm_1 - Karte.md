# Karte
* * *

# Welche Karte wird benutzt?
Die Art der Karte ist irrelevant.
Wichtig ist, dass sie 1 oder 2 Feldarten hat, (entweder einheitliche Felder oder 2 verschiedene Arten von Feldern (eine für Infanterie und Infrastruktur, eine für Navy und Air Force))
und dass es einfach ist, die Farben der Felder zu ändern, um einer anderen Nation ein Feld zu übergeben / besetzen zu lassen!
Je nach Größe der Karte, solltet ihr auch die Regeln für Geschwindigkeit und Infrastruktur anpassen.

Das offizielle Spiel benutzt die Wargames-Karte von Mapchart, (`mapchart.net/wargames.html`,) die modifiziert wurde, um bsw. Zonen anzuzeigen und es wurde eine zweite art an Feldern hinzugefügt, die Flugzeuge und die Navy nutzen.

Für bsw. Brettspiele, empfehle ich Plättchen oder Kärtchen in Form der Felder mit den Farben aller Nationen zu benutzen und sie auf die leeren (weißen) Felder zu legen!

# Wie bewegt man Divisionen?
Für bsw. Brettspiele ist diese Frage natürlich kein Problem.

Das offizielle Spiel benutzt ein Bildbearbeitungstool (GIMP) um auf der Karte Divisionen, Infrastruktur, usw. zu platzieren und live zu bewegen. (natürlich haben wir einen Spielemanager, der die Karte verwaltet!)

# Private & Öffentliche Karten
Je nach Spielart, kann die Karte Privat oder Öffentlich sein,
bei einem Brettspiel beispielsweise, ist es nicht möglich eine private Karte zu nutzen, da jeder Spieler alle Einheiten, Gebäude usw. einsehen kann!
die private Kartenart zeigt jedem Spieler an, welche Felder zu welcher Nation gehören, zeigt jedoch nicht die Einheiten, Gebäude usw.!
Einheiten und Truppen sind nur sichtbar, wenn:
```yaml
- Frontlinie: Jede Runde 1d6, 6: eine Division kann 3 angrenzende Divisionen anzeigen!
- Flugzeuge: Flugzeuge sind in der Reichweiter eines anderen, oder(!) In ihrer Reichweite ist eine Truppe oder ein Schiff!
- Shiffe: Schiffe sind in der gleichen Reichweite eines Flugzeugs oder an einer Küste mit gebäuden oder Truppen!
```
Es gibt noch eine dritte Kartenart, Fog of War (Nebel des Krieges) - diese zeigt dem Spieler nur Felder in einem bestimmten Umkreis um seine Felder und Truppen herum!
Das offizielle Spiel benutzt eine öffentliche Karte.

# Tipp für Management!
Eine Karte für jeden Spieler, mit beispielsweise:
```diff
- NATION MUSTERMANN
- Fraktion: Neutral

+ Mannstärke:		XXX
+ Bauarbeiter:		XXX
+ Gebäude zerstört:	XXX

- Cooldowns:
* GEBÄUDE-XY kann gebaut werden in: XXX Runden

+ Arbeiten:
* DIVISION-YZ ist fertiggestellt in: XXX Runden

etc. ...
```
wäre Hilfreich für die Übersicht für den Spieler und die Verwaltung!

# Kanäle
In den Raum zwischen Feldern kann ein Kanal errichtet werden. Dieser kann maximal 3 Segmente lang sein.
Der Segmentbau dauert 20 Runden.
Danach agiert der Zwischenraum als ein Wasserfeld.
Ein Kanal kann zurückgebaut werden, dies dauert 10 Runden.
