# Angriff
* * *
[X] - Eigene Division
[O] - Gegn. Division
[H] - Dritte Gegn. Division
[V] - Freundl. Division

Hat eine Infanterie-basierte Division keine Waffen, kann sie nicht kämpfen.

## [X]→[O]

Formel:
`[X] angriff - [O] verteidigung = W`
```diff
wenn:

- negativ: [X] fehlschlag, [X] leben - +W%, [X] angriff - 10% (1R)

+ positiv/gleich: [O] rückzug, [O] leben - W + [X] offensive %

(rr+6)
```

### Basis-Schaden (an jede Partei)
```diff
Würfel 1d6:

1 - 3:	B = 2.5%
4:	B = 5%
5-6:	B = 1-5% (Würfel: 6=1)
```
[[]] leben - B

```diff
2x Würfel 1d6

Würfel 1 , Würfel 2 -> Anzahl an verbrauchten Waffen (bei allen Infanterie-basierten Divisionen)
(Minimum: 11; Maximum: 66)
```

### Beide Divisionen greifen sich gegenseitig an, [X]→←[O]
```yaml
				höhere Geschwindigkeit greift an,
wenn gleich, 	höhere Offensive greift an, 
wenn gleich, 	Münzwurf!
```
**2. Schlacht (Wiederholung) -** vorherige Verteidigung greift an!

# Dreiecksangriff:
```yaml
[X]→[O]→[H]
Münzwurf!

... [X]←[H]
33% Chance;
1%: umgekehrter Angriff!

(4 Parteien --> 25% Chance usw. ...)
```

# Angriff von mehreren Seiten
(hier gehen wir von hexagonalen Feldern aus!)

Angriff von:
```
2 gleicher Front:
- angriff + 10%
3 gleicher Front:
- angriff + 20%

1x gegenüberl. :
- angriff + 20%
- offensive + 25%
- Tod bei verlust

2x gegenüberl. :
- angriff + 40%
- offensive + 40%
- Tod bei verlust

100% aller Seiten:
- angriff + 30%
- Tod bei verlust
```
