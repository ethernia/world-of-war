# Doktrien [Plugin]
* * *
Plugin by: *Official World of War*
v1.0.0
* * *

Infanterie, Navy und Air Force haben ihre eigenen Taktiken.
Jede Nation wählt eine Taktik jeder Klasse bei Beginn des Spiels.

# Infanterie
## Feuerkraft
Panzer & Motor.:
- Angriff +1

Panzer Ausbilden:
- - 5 Mannstärke
- - 1 R

Belieferung:
- + 1 Entfernung zu einer Straße

Defensive Linien:
- Zerstört bei Betreten

## Grabenkrieg
Infanterie:
- Leben +20

Kavallerie:
- Angriff +1

Belagerung:
- -50% Defizite

kann Defensive Linien erstellen

## Blitzkrieg
Infanterie:
- Geschwindigkeit +1
- Offensive +2

Miliz:
- Angriff +1

Belagerung:
- +2 Straßen Abstand z/ Lieferketten

* * *

# Navy
## Überlegenheit
Zerstörer:
- Offensive +1

Korvetten:
- Offensive +1
- Leben +10

## Mobilität
Träger:
- Angriff +3
- Offensive +2

Fregatte:
- Geschwindigkeit +1

## Handelsunterbrechung
Träger:
- Angriff +1
- Offensive +2

U-Boote:
- Geschwindigkeit +1
- Offensive +1

* * *

# Air Force
## Überlegenheit
Jäger:
- Geschwindigkeit +1
- Leben +20

Schlachtflugzeug:
- Offensive +1

## Zerstörung
Bomber:
- Offensive +1

Bomben:
```diff
Würfel 1d6

1 - 2:	Infrastruktur zerstört, Divisionen auf Feld leben - 20% {Reperatur -1R}
3 - 4:	Infrastruktur zerstört, Divisionen auf Feld leben - 75%
5 - 6:	Infrastruktur zerstört, Divisionen auf Feld leben - 100% {Navy-Division zerstört}
```
## Unterstützung
Schlachtflugzeug:
- Offensive +1

Transportflugzeug:
- Offensive +1

Unterstützung:
*Jede Runde:*
```diff
Würfel 1d6:

2 - 4:	Zufällige eigene Division angriff	+10%(1R)
1 & 5:	Zufällige gegnerische Division leben	-15%
6:	Zufällige gegnerische Division leben	-30%
```
