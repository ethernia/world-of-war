# Infrastruktur
* * *
(i:) *Eine Kette besteht aus einer Stadt und weiteren Gebäuden, eine Kette mit Zivilfabrik wäre eine Stadt und eine Zivilfabrik, verbunden mit Straßen*

** = in die Nationale Reserve! (bei Handel sofort übertragen, kann nur über Verhandlungen verloren werden)

# Arten von Gebäuden
## Straße `| - |`
Eine Straße Verbindet eine Infrastruktur mit einer anderen.
Baut man jedes andere Stück Infrastruktur, wird automatisch eine Straße an diese Stelle gebaut.
Straßen sind die einzige Art Infrastruktur, die auf anderen platziert werden können. (*dazu gelten keine Ports, da sie nicht auf regulären Feldern stehen*)
```yaml
geschwindigkeit + 1
(nur innerhalb des landes, von Straße zu Straße!)
```

#### Brücke `| -- |`
Eine Brücke ist eine Straße, nur ist sie ein Verbindungsstück zwischen Insel und Insel. (es kann nur über Flüssen oder direkt von Insel zu Insel oder Insel zu Küste gebaut werden und das nur an die dichteste Insel, um eine Kette zu erstellen. das Imperium Insulae kann keine Brücken zwischen der Insula Imperii und anderen Inseln/Küsten bauen.)
(i:) *Wenn ihr mit 2 verschiedenen Feldarten spielt, nutzt eine Brücke ein Meeres-Feld, spielt ihr mit nur einer Feldart, ist es so groß, wie eine reguläre Straße!*

## Stadt `| S |`
(i:) *Jede Hauptstadt ist automatisch eine Metropole*
Eine Stadt ist der Wichtigste Teil der Infrastruktur.
Was genau Städte für Werte haben, hängt von dem Level einer Stadt ab. Siehe die Städte-Katergorie!
Das Errichten einer Stadt verbraucht `1 Stahl`

## Kaserne `| X |`
In einer Kaserne können alle Divisionen unter der Infanterie-Katergorie ausgebildet werden.
**Für das Ausbilden ist eine Kette mit Kriegsfabrik benötigt.**
Das Errichten einer Kaserne verbraucht `1 Stahl`

## Port `| H |`
In einem Port können alle Divisionen unter der Navy-Kategorie ausgebildet werden.
**Für das Ausbilden ist eine Kette mit Kriegsfabrik und Kaserne benötigt.**
In einem Port können bis zu 10 Divisionen stationiert sein.
(i:) *Wenn ihr mit 2 verschiedenen Feldarten spielt, steht ein Port nicht auf einem Feld, sondern auf einem Meeres-Feld, angrenzend an einem Feld, spielt ihr mit nur einer Feldart, steht ein Port an der Küste eines Feldes, kann also auch auf dem gleichen Feld, wie ein anderes Gebäude stehen!*
Das Errichten eines Ports verbraucht `1 Stahl`

## Flughafen `| ^ |`
In einem Flughafen können alle Divisionen unter der Air Force-Kategorie ausgebildet werden.
**Für das Ausbilden ist eine Kette mit Kriegsfabrik und Kaserne benötigt.**
In einem Flughafen können bis zu 10 Divisionen stationiert sein.
Das Errichten eines Flughafens verbraucht `1 Stahl`

## Farm `| Ú |`
Einer Farm kann ein Bauarbeiter zugewiesen werden.
Dieser Produziert dann `1 Weizen / Runde`

## Kohlemine `| Ü |`
Einer Kohlemine kann ein Bauarbeiter zugewiesen werden.
Dieser Produziert dann `1 Kohle / Runde`

## Stahlwerk `| Ö |`
Einem Stahlwerk kann ein Bauarbeiter zugewiesen werden.
Bei einer Kette mit Kraftwerk produziert dieser dann `1 Stahl / 2 Runden`, dies verbraucht 1 Kohle.

## Zivilfabrik `| U |`
Um Teil einer Kette zu sein, darf die Zivilfabrik maximal 5 Straßen entfernt sein.
Eine Zivilfabrik kann, bei einer Kette mit Kraftwerk, folgendes herstellen:
- 1 Medizin / 3 Runden (bei anliegendem Transporter) (verbraucht 2 Weizen)
- Nahrungsmittel für bis zu 5 Städte (verbraucht 1 Weizen)

## Kriegsfabrik `| O |`
Um Teil einer Kette zu sein, darf die Kriegsfabrik maximal 5 Straßen entfernt sein.
Eine Kriegsfabrik kann, bei einer Kette mit Kraftwerk, folgendes herstellen:
- 50 Waffen / 5 Runden (bei anliegendem Transporter) (verbraucht 1 Stahl und 1 Kohle)
- 1 Panzer / 5 Runden** (verbraucht 7 Stahl und 1 Kohle)
- 2 Autos / 5 Runden** (verbraucht 5 Stahl und 1 Kohle)
- 1 Jäger / 5 Runden** (verbraucht 7 Stahl und 1 Kohle)
- 1 Bomber / 5 Runden** (verbraucht 7 Stahl und 1 Kohle)
- 1 Schlachtflgzg. / 5 Runden** (verbraucht 7 Stahl und 1 Kohle)
- 1 Transportflgzg. / 5 Runden** (verbraucht 7 Stahl und 1 Kohle)

## Werft `| C |`
Um Teil einer Kette zu sein, darf die Werft maximal 5 Straßen entfernt sein.
Eine Werft kann, bei einer Kette mit Kraftwerk, folgendes herstellen:
- 1 Zerstörer / 10 Runden** (verbraucht 10 Stahl und 1 Kohle)
- 1 Korvette / 10 Runden** (verbraucht 7 Stahl und 1 Kohle)
- 1 Fregatte / 10 Runden** (verbraucht 10 Stahl und 1 Kohle)
- 1 U-Boot / 15 Runden** (verbraucht 10 Stahl und 1 Kohle)
- 1 Landungsschiff / 15 Runden** (verbraucht 10 Stahl und 1 Kohle)
- 1 Flugzeugträger / 15 Runden** (verbraucht 10 Stahl und 1 Kohle)
- 1 Konvoi / 5 Runden** (verbraucht 5 Stahl und 1 Kohle)
- 1 Kanalbauschiff / 15 Runden** (verbraucht 15 Stahl und 2 Kohle)

## Kraftwerk `| V |`
Um Teil einer Kette zu sein, darf das Kraftwerk maximal 7 Straßen entfernt sein.
Alle 10 Runden benötigt das Kraftwerk `1 Kohle` um weiter zu laufen.

## Öl-Raffinerie `| ' |`
Um Teil einer Kette zu sein, darf die Öl-Raffinerie maximal 5 Straßen entfernt sein.
Eine Öl-Raffinerie gibt 10 Öl an einen anliegenden Transporter.
Das Errichten einer Stadt verbraucht `1 Stahl`

## Öl-Silo `| # |`
Um Teil einer Kette zu sein, darf die Öl-Raffinerie maximal 5 Straßen entfernt sein.
Ein Öl-Silo hat 100 Ölslots.

## Bunker `| = |`
```yaml
verteidigung + 20%
offensive - 5%
```

## Lazarett `| + |`
In einem Lazarett können Divisionen geheilt werden.
Ein Lazarett kann nicht an einer Frontlinie erbaut werden, steht es bereits dort, hat es keinen Effekt mehr.
Divisionen benötigen eine Runde nach dem heilen um mobilisiert zu werden.
Es fügt jede Runde 10% leben hinzu und verbraucht dabei eine Medizin!.
Es kann bis zu 10 Medizin lagern.

# Städte
(i:) *Die Mannstärke und das Mannstärke-Limit wird erst bei Kette mit einer Nahrungs-herstellenden Zivilfabrik frei.*
*Die Bauarbeiter werden erst bei Kette mit Öl-Silo und Zivilfabrik frei.*
*Ist die Mannstärke über dem Limit, stirbt alles über dem Limit in 2 Runden!*
### Dorf `| S1 |`
```diff
+ +2/R Mannstärke
+ +25 Mannstärke-Limit
```
### Kleinstadt `| S2 |`
```diff
+ +25 Mannstärke-Limit
```
### Großstadt `| S3 |`
```diff
+ +2/R Mannstärke
+ +50 Mannstärke-Limit
+ +1 Bauarbeiter
```
### Metropole `| S4 |`
```diff
+ +2/R Mannstärke
+ +50 Mannstärke-Limit
+ +2 Bauarbeiter
- verteidigung + 5%
- gegner. angriff - 5% bei Navy-Invasionen (Navy und Infanterie!)
```
Das Aufwerten einer Stadt verbraucht `1 Stahl` und `1 Kohle`

# Gebäude bauen
Gebäude können auf anderen Gebäuden gebaut werden, um die alten zu ersetzen.
Brücken können mit einem freien Bauarbeiter sofort zerstört werden.
Jede Nation startet mit 3 Bauarbeitern durch die Hauptstadt.
Nationen können maximal 20 Bauarbeiter haben.
```yaml
Bauzeit in Runden:
Straßen	Gebäude	(ohne Straße)	Upgrades:	Kleinstadt	Großstadt	Metropole
2	6	4				4		4		5
```

# Gebäude reparieren
Reperaturprojekte benötigen ebenfalls Bauarbeiter.
Das Gebäude ist nutzlos, ohne Reperatur.
(i:) *Mannstärke, Bauarbeiter und vorhandene, erstellte Divisionen bleiben bestehen!*
(Minimale Reperaturzeit: zwei Runde)
Das Reparieren eines Gebäudes benötigt ebenfalls `1 Kohle` und `1 Stahl`
```yaml
Reperaturzeit in Runden:
Infr:	Straßen	Gebäude Pro Stadt-Upgrade
2	4	+1
```
