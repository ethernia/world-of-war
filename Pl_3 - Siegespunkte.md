# Siegespunkte
* * *
Siegespunkte sind:

- Vor dem Spiel auf der Karte zu verteilen
- Städte
- Ports
- Flughafen

Ein Krieg ist automatisch gewonnen, wenn eine Partei über 80% der Siegespunkte des Gegners kontrolliert.

Hauptstädte sind 3 Siegespunkte wert!
