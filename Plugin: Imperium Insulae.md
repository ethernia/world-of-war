# Imperium Insulae [Plugin]
* * *
Plugin by: *Official World of War*
v3.0.0
* * *
1 Insel Challenge
Das Imperium Insulae ist ein Ein-Insel Staat.
Die Herausforderung ist es, mit der Insula Imperii als Hauptstadt die Welt zu erobern.
Die Hauptstadt ist standardgemäß Verteidigt, aber allein der Anfang ist Schwer.
Keine Allianzen, alle sind deine Feinde!
Keine Friedensverhandlungen, was du besetzt, hast du!

# 1 Insel Challenge
* * *
# Insula Imperii
Die Hauptstadt des Imperiums bleibt die Insula Imperii!
Die Insula Imperii kann nicht belagert werden und nicht von Schiffen blockiert werden!

# Start
Auf der Insula Imperii ist ein Port (im offiziellen Spiel im Osten)

Das Imperium Insulae startet mit 5 Panzerdivisionen ( [  U  ] ), 10 Infanteriedivisionen ( [  X  ] ), einem Truppentransporter und 5 Korvetten ( [< = >] & [< / >] )
Die Panzerdivision steht auf der Insula Imperii, Truppentransporter und Korvette sind im Dock Insula Imperiis gedockt.

# Tod
Sind alle Divisionen des Imperium Insulae besiegt, wird das gesamte Imperium Insulae nach einer Runde zum Start zurückgesetzt

# Verhandlungen
Das Imerpium Insulae ist nicht dazu in der Lage zu verhandeln.
Daher gilt auch: Das besetzte Land gehört dem Imperium und zählt als Kolonie!
Das Imperium Insulae kann nicht an Friedensverhandlungen teilnehmen, Friedensverhandlungen ignorieren das Imperium Insulae. Wer das Land eines neutralen oder feindlichen Spielers angreift, welches jedoch vom Imperium Insulae besetzt wird, erklärt dieser Nation krieg.
Das Imperium Insulae kann offiziell ebenfalls nicht mit anderen Spielern um bsw. Land oder Allianzen verhandeln, jedoch kann man sich miteinander absprechen und so bsw. einen Waffenstillstand, die Übergabe von Land oder einen gemeinsamen Angriff auf eine Nation verhandeln, dies kann aber ohne Konsequenzen ignoriert werden.

# Weltherrschaft
Sobald das Imperium Insulae jedes Feld auf der gesamten Weltkarte besetzt, ist das Spiel beendet.
